package ofa;


import java.util.concurrent.Callable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import ofa.rabbit.exceptions.RabbitConnectionException;

public class OFARabbitMQClient<T> implements Callable<Boolean> {
	private String qName;
	private String hostIp;
	private int port;
	private String userName;
	private String password;
	private Boolean isDurable = false;
	private boolean isSetupSuccess;
	private T message;
	Connection connection =null;

	ObjectMapper mapper=new ObjectMapper();
	

	public  OFARabbitMQClient(String qName, String hostIp, int port, String userName, String password,T message,
			Boolean isDurable) {
		this.qName = qName;
		this.hostIp = hostIp;
		this.port = port;
		this.userName = userName;
		this.password = password;
        this.message=message;
		if (isDurable != null)
			this.isDurable = isDurable;

		try {
			setUp();
			isSetupSuccess = true;
		} catch (RabbitConnectionException e) {
			isSetupSuccess = false;
			System.err.println("Error : " + e.getMessage());
		}
	}

	public Boolean call() throws Exception {
		// TODO Auto-generated method stub
		boolean flag = false;
		try {
			if (isSetupSuccess) {
				

				Channel channel = connection.createChannel();

				channel.queueDeclare(qName, isDurable, false, false, null);
				
					String message = mapper.writeValueAsString(this.message);
					channel.basicPublish("", qName, null, message.getBytes());

					System.out.println(" [x] Sent '" + message + "'");
				
				
				channel.close();
			    connection.close();
				flag = true;
			}
		} catch (Exception e) {
			System.out.println("ff " + e.getMessage());
		}
		return flag;
	}

	private void setUp() throws RabbitConnectionException {
		try {

			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(this.hostIp);
			factory.setPort(this.port);
			factory.setUsername(this.userName);
			factory.setPassword(this.password);
		    this.connection = factory.newConnection();
		} catch (Exception e) {
			throw new RabbitConnectionException("Exception while doing setup reason may be : " + e.getMessage());
		}
	}

}
