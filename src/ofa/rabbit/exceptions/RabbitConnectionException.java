package ofa.rabbit.exceptions;

public class RabbitConnectionException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6283948078782800838L;
	private String message;
	
	public RabbitConnectionException(String message) {
		this.message=message;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return this.message;
	}
	
}
