package ofa;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import ofa.rabbit.exceptions.RabbitConnectionException;

public class RecommandationRabbitMQConsumer{
	private String qName;
	private String hostIp;
	private int port;
	private String userName;
	private String password;
	private Boolean isDurable = false;
	private boolean isSetupSuccess;
	private ConsumerBroker broker;
	
	Connection connection =null;
	ObjectMapper mapper=new ObjectMapper();
	
	public  RecommandationRabbitMQConsumer(String qName, String hostIp, int port, String userName, String password,
			Boolean isDurable,ConsumerBroker broker) {
		this.qName = qName;
		this.hostIp = hostIp;
		this.port = port;
		this.userName = userName;
		this.password = password;
		this.broker=broker;
		if (isDurable != null)
			this.isDurable = isDurable;

		try {
			setUp();
			isSetupSuccess = true;
		} catch (RabbitConnectionException e) {
			isSetupSuccess = false;
			System.err.println("Error : " + e.getMessage());
		}
		
		if(isSetupSuccess)
			onConsumer();
	}
	
	
	public void onConsumer(){
		   try{
		    Channel channel = connection.createChannel();

		    channel.queueDeclare(qName, isDurable, false, false, null);
		    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
		   
		    Consumer consumer = new DefaultConsumer(channel) {
		    	  @Override
		    	  public void handleDelivery(String consumerTag, Envelope envelope,
		    	                             AMQP.BasicProperties properties, byte[] body)
		    	      throws IOException {
		    	    String message = new String(body, "UTF-8");
		    	    
		    	    try{
		    	        broker.consumeMsg(message,qName);
		    	    }catch(Exception ex){
		    	    	
		    	    }
		    	    System.out.println(" [x] Received '" + message + "'");
		    	  }
		    	};
		    	channel.basicConsume(qName, true, consumer);
		    }catch(Exception e){
		    	System.out.println("cc "+e.getMessage());
		    }
	}
	
	
	private void setUp() throws RabbitConnectionException {
		try {

			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(this.hostIp);
			factory.setPort(this.port);
			factory.setUsername(this.userName);
			factory.setPassword(this.password);
		    this.connection = factory.newConnection();
		} catch (Exception e) {
			throw new RabbitConnectionException("Exception while doing setup reason may be : " + e.getMessage());
		}
	}
}
