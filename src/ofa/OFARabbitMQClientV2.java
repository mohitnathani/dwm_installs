package ofa;

import java.util.concurrent.Callable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import ofa.rabbit.exceptions.RabbitConnectionException;

public class OFARabbitMQClientV2<T> implements Callable<Boolean>{

	

	private static Connection connection =null;
	private Boolean isDurable = false;
	private String qName;
	private T message;


	ObjectMapper mapper=new ObjectMapper();
	
	

	public  OFARabbitMQClientV2(String qName, String hostIp, int port, String userName, String password,T message,
			Boolean isDurable) {

		this.qName = qName;
	
	
	
	
        this.message=message;
		if(connection == null){
		try {
			setUp(qName, hostIp,  port, userName, password,
					 isDurable);
			
			
		} catch (RabbitConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	/*	try {
			//setUp();
			isSetupSuccess = true;
		} catch (RabbitConnectionException e) {
			isSetupSuccess = false;
			System.err.println("Error : " + e.getMessage());
		}*/
	}
	
	
/*	public static   OFARabbitMQClientV2 getInstance(String qName, String hostIp, int port, String userName, String password,T message,
			Boolean isDurable){
		if(obj==null){
			obj=new OFARabbitMQClientV2<T>(qName, hostIp,  port, userName, password,message,
					 isDurable);
		}
		return obj;
	}*/
	
	
	
	private static void setUp(String qName, String hostIp, int port, String userName, String password,
			Boolean isDurable) throws RabbitConnectionException {
	
		try {

			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(hostIp);
			factory.setPort(port);
			factory.setUsername(userName);
			factory.setPassword(password);
		    connection = factory.newConnection();
		} catch (Exception e) {
			throw new RabbitConnectionException("Exception while doing setup reason may be : " + e.getMessage());
		}		
		
	}
	
	
	public Boolean call() throws Exception {
		// TODO Auto-generated method stub
		boolean flag = false;
		try {
		
				

				Channel channel = connection.createChannel();

				channel.queueDeclare(qName, isDurable, false, false, null);
				
					String message = mapper.writeValueAsString(this.message);
					channel.basicPublish("", qName, null, message.getBytes());

					System.out.println(" [x] Sent '" + message + "'");
				
				
				channel.close();
			    connection.close();
				flag = true;
			
		} catch (Exception e) {
			System.out.println("ff " + e.getMessage());
		}
		return flag;
	}
	
	
	
}
