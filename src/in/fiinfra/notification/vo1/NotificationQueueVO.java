package in.fiinfra.notification.vo1;

import java.io.Serializable;
import java.sql.Timestamp;

public class NotificationQueueVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer notificationQueueId;
	private Integer templateType;
	private String templateXML;
	private Integer notificationTarget;
	private Integer notificationChanneTtype;
	private Integer ownerType;
	private Integer ownerId;
	private String payloadxml;
	private Integer attachment1DocumentID;
	private String attachmentPath1;
	private Integer attachment2DocumentID;
	private String attachmentPath2;
	private Integer attachment3DocumentID;
	private String attachmentPath3;
	private Integer priority;
	private Integer useSignature;
	private Integer buId;
	private Integer templateId;
	private String signatureText;
	private String logoPath;
	private String fromEmailID;
	private String fromEmailName;
	private String toEmailIDSMSMobileNo;
	private String cCEmailID;
	private String bCCEmailID;
	private String emailSubjectText;
	private String emailSMSBodyText;
	private String addSignatureText;
	private String addDisclaimerText;
	private Integer deliveryStatusID;
	private Integer deliveryAttemptCount;
	private String deliveryLastErrorText;
	private Timestamp lastAttemptDTTM;
	private String replyToEmailID;
	private String jobStatus;
	
	private int notifyEventId;
	
	private Integer tcSignOffFlag;
	private Integer clientAppFlag;
	private Integer smsRouteFlag;
	
	public String getReplyToEmailID() {
		return replyToEmailID;
	}

	public void setReplyToEmailID(String replyToEmailID) {
		this.replyToEmailID = replyToEmailID;
	}

	public Integer getNotificationQueueId() {
		return notificationQueueId;
	}

	public void setNotificationQueueId(Integer notificationQueueId) {
		this.notificationQueueId = notificationQueueId;
	}

	public Integer getTemplateType() {
		return templateType;
	}

	public void setTemplateType(Integer templateType) {
		this.templateType = templateType;
	}

	public String getTemplateXML() {
		return templateXML;
	}

	public void setTemplateXML(String templateXML) {
		this.templateXML = templateXML;
	}

	public Integer getNotificationTarget() {
		return notificationTarget;
	}

	public void setNotificationTarget(Integer notificationTarget) {
		this.notificationTarget = notificationTarget;
	}

	public Integer getNotificationChanneTtype() {
		return notificationChanneTtype;
	}

	public void setNotificationChanneTtype(Integer notificationChanneTtype) {
		this.notificationChanneTtype = notificationChanneTtype;
	}

	public Integer getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(Integer ownerType) {
		this.ownerType = ownerType;
	}

	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	public String getPayloadxml() {
		return payloadxml;
	}

	public void setPayloadxml(String payloadxml) {
		this.payloadxml = payloadxml;
	}

	public Integer getAttachment1DocumentID() {
		return attachment1DocumentID;
	}

	public void setAttachment1DocumentID(Integer attachment1DocumentID) {
		this.attachment1DocumentID = attachment1DocumentID;
	}

	public String getAttachmentPath1() {
		return attachmentPath1;
	}

	public void setAttachmentPath1(String attachmentPath1) {
		this.attachmentPath1 = attachmentPath1;
	}

	public Integer getAttachment2DocumentID() {
		return attachment2DocumentID;
	}

	public void setAttachment2DocumentID(Integer attachment2DocumentID) {
		this.attachment2DocumentID = attachment2DocumentID;
	}

	public String getAttachmentPath2() {
		return attachmentPath2;
	}

	public void setAttachmentPath2(String attachmentPath2) {
		this.attachmentPath2 = attachmentPath2;
	}

	public Integer getAttachment3DocumentID() {
		return attachment3DocumentID;
	}

	public void setAttachment3DocumentID(Integer attachment3DocumentID) {
		this.attachment3DocumentID = attachment3DocumentID;
	}

	public String getAttachmentPath3() {
		return attachmentPath3;
	}

	public void setAttachmentPath3(String attachmentPath3) {
		this.attachmentPath3 = attachmentPath3;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getUseSignature() {
		return useSignature;
	}

	public void setUseSignature(Integer useSignature) {
		this.useSignature = useSignature;
	}

	public Integer getBuId() {
		return buId;
	}

	public void setBuId(Integer buId) {
		this.buId = buId;
	}

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getSignatureText() {
		return signatureText;
	}

	public void setSignatureText(String signatureText) {
		this.signatureText = signatureText;
	}

	public String getLogoPath() {
		return logoPath;
	}

	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	public String getFromEmailID() {
		return fromEmailID;
	}

	public void setFromEmailID(String fromEmailID) {
		this.fromEmailID = fromEmailID;
	}

	public String getFromEmailName() {
		return fromEmailName;
	}

	public void setFromEmailName(String fromEmailName) {
		this.fromEmailName = fromEmailName;
	}

	public String getToEmailIDSMSMobileNo() {
		return toEmailIDSMSMobileNo;
	}

	public void setToEmailIDSMSMobileNo(String toEmailIDSMSMobileNo) {
		this.toEmailIDSMSMobileNo = toEmailIDSMSMobileNo;
	}

	public String getcCEmailID() {
		return cCEmailID;
	}

	public void setcCEmailID(String cCEmailID) {
		this.cCEmailID = cCEmailID;
	}

	public String getbCCEmailID() {
		return bCCEmailID;
	}

	public void setbCCEmailID(String bCCEmailID) {
		this.bCCEmailID = bCCEmailID;
	}

	public String getEmailSubjectText() {
		return emailSubjectText;
	}

	public void setEmailSubjectText(String emailSubjectText) {
		this.emailSubjectText = emailSubjectText;
	}

	public String getEmailSMSBodyText() {
		return emailSMSBodyText;
	}

	public void setEmailSMSBodyText(String emailSMSBodyText) {
		this.emailSMSBodyText = emailSMSBodyText;
	}

	public String getAddSignatureText() {
		return addSignatureText;
	}

	public void setAddSignatureText(String addSignatureText) {
		this.addSignatureText = addSignatureText;
	}

	public String getAddDisclaimerText() {
		return addDisclaimerText;
	}

	public void setAddDisclaimerText(String addDisclaimerText) {
		this.addDisclaimerText = addDisclaimerText;
	}

	public Integer getDeliveryStatusID() {
		return deliveryStatusID;
	}

	public void setDeliveryStatusID(Integer deliveryStatusID) {
		this.deliveryStatusID = deliveryStatusID;
	}

	public Integer getDeliveryAttemptCount() {
		return deliveryAttemptCount;
	}

	public void setDeliveryAttemptCount(Integer deliveryAttemptCount) {
		this.deliveryAttemptCount = deliveryAttemptCount;
	}

	public String getDeliveryLastErrorText() {
		return deliveryLastErrorText;
	}

	public void setDeliveryLastErrorText(String deliveryLastErrorText) {
		this.deliveryLastErrorText = deliveryLastErrorText;
	}

	public Timestamp getLastAttemptDTTM() {
		return lastAttemptDTTM;
	}

	public void setLastAttemptDTTM(Timestamp lastAttemptDTTM) {
		this.lastAttemptDTTM = lastAttemptDTTM;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public int getNotifyEventId() {
		return notifyEventId;
	}

	public void setNotifyEventId(int notifyEventId) {
		this.notifyEventId = notifyEventId;
	}

	public Integer getTcSignOffFlag() {
		return tcSignOffFlag;
	}

	public void setTcSignOffFlag(Integer tcSignOffFlag) {
		this.tcSignOffFlag = tcSignOffFlag;
	}

	public Integer getClientAppFlag() {
		return clientAppFlag;
	}

	public void setClientAppFlag(Integer clientAppFlag) {
		this.clientAppFlag = clientAppFlag;
	}

	public Integer getSmsRouteFlag() {
		return smsRouteFlag;
	}

	public void setSmsRouteFlag(Integer smsRouteFlag) {
		this.smsRouteFlag = smsRouteFlag;
	}

	@Override
	public String toString() {
		return "NotificationQueueVO [notificationQueueId=" + notificationQueueId + ", templateType=" + templateType
				+ ", templateXML=" + templateXML + ", notificationTarget=" + notificationTarget
				+ ", notificationChanneTtype=" + notificationChanneTtype + ", ownerType=" + ownerType + ", ownerId="
				+ ownerId + ", payloadxml=" + payloadxml + ", attachment1DocumentID=" + attachment1DocumentID
				+ ", attachmentPath1=" + attachmentPath1 + ", attachment2DocumentID=" + attachment2DocumentID
				+ ", attachmentPath2=" + attachmentPath2 + ", attachment3DocumentID=" + attachment3DocumentID
				+ ", attachmentPath3=" + attachmentPath3 + ", priority=" + priority + ", useSignature=" + useSignature
				+ ", buId=" + buId + ", templateId=" + templateId + ", signatureText=" + signatureText + ", logoPath="
				+ logoPath + ", fromEmailID=" + fromEmailID + ", fromEmailName=" + fromEmailName
				+ ", toEmailIDSMSMobileNo=" + toEmailIDSMSMobileNo + ", cCEmailID=" + cCEmailID + ", bCCEmailID="
				+ bCCEmailID + ", emailSubjectText=" + emailSubjectText + ", emailSMSBodyText=" + emailSMSBodyText
				+ ", addSignatureText=" + addSignatureText + ", addDisclaimerText=" + addDisclaimerText
				+ ", deliveryStatusID=" + deliveryStatusID + ", deliveryAttemptCount=" + deliveryAttemptCount
				+ ", deliveryLastErrorText=" + deliveryLastErrorText + ", lastAttemptDTTM=" + lastAttemptDTTM
				+ ", replyToEmailID=" + replyToEmailID + ", jobStatus=" + jobStatus + ", notifyEventId=" + notifyEventId
				+ ", tcSignOffFlag=" + tcSignOffFlag + ", clientAppFlag=" + clientAppFlag + ", smsRouteFlag="
				+ smsRouteFlag + "]";
	}

}
