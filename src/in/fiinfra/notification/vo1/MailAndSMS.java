package in.fiinfra.notification.vo1;

public class MailAndSMS {
	private String emailFromName;
	private String emailFrom;
	private String emailPassword;
	private String emailHost;
	private int mailPort;
	private String smtpAuthEnabled;
	private String smtpStartTLSenabled;
	private String imageURL;
	private String attachmentpath;
	private String mailProtocol;
	private String prefix;
	private String smsPromoUserName;
	private String smsPromoPassword;
	private String smsPromoSID;
	
	private String smsTransUserName;
	private String smsTransPassword;
	private String smsTransSID;
	
	public String getEmailFromName() {
		return emailFromName;
	}
	public void setEmailFromName(String emailFromName) {
		this.emailFromName = emailFromName;
	}
	public String getEmailFrom() {
		return emailFrom;
	}
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	public String getEmailPassword() {
		return emailPassword;
	}
	public void setEmailPassword(String emailPassword) {
		this.emailPassword = emailPassword;
	}
	public String getEmailHost() {
		return emailHost;
	}
	public void setEmailHost(String emailHost) {
		this.emailHost = emailHost;
	}
	public int getMailPort() {
		return mailPort;
	}
	public void setMailPort(int mailPort) {
		this.mailPort = mailPort;
	}
	public String getSmtpAuthEnabled() {
		return smtpAuthEnabled;
	}
	public void setSmtpAuthEnabled(String smtpAuthEnabled) {
		this.smtpAuthEnabled = smtpAuthEnabled;
	}
	public String getSmtpStartTLSenabled() {
		return smtpStartTLSenabled;
	}
	public void setSmtpStartTLSenabled(String smtpStartTLSenabled) {
		this.smtpStartTLSenabled = smtpStartTLSenabled;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	public String getAttachmentpath() {
		return attachmentpath;
	}
	public void setAttachmentpath(String attachmentpath) {
		this.attachmentpath = attachmentpath;
	}
	public String getMailProtocol() {
		return mailProtocol;
	}
	public void setMailProtocol(String mailProtocol) {
		this.mailProtocol = mailProtocol;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getSmsPromoUserName() {
		return smsPromoUserName;
	}
	public void setSmsPromoUserName(String smsPromoUserName) {
		this.smsPromoUserName = smsPromoUserName;
	}
	public String getSmsPromoPassword() {
		return smsPromoPassword;
	}
	public void setSmsPromoPassword(String smsPromoPassword) {
		this.smsPromoPassword = smsPromoPassword;
	}
	public String getSmsPromoSID() {
		return smsPromoSID;
	}
	public void setSmsPromoSID(String smsPromoSID) {
		this.smsPromoSID = smsPromoSID;
	}
	public String getSmsTransUserName() {
		return smsTransUserName;
	}
	public void setSmsTransUserName(String smsTransUserName) {
		this.smsTransUserName = smsTransUserName;
	}
	public String getSmsTransPassword() {
		return smsTransPassword;
	}
	public void setSmsTransPassword(String smsTransPassword) {
		this.smsTransPassword = smsTransPassword;
	}
	public String getSmsTransSID() {
		return smsTransSID;
	}
	public void setSmsTransSID(String smsTransSID) {
		this.smsTransSID = smsTransSID;
	}

	

}
